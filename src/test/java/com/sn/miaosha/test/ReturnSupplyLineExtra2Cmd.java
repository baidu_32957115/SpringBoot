package com.sn.miaosha.test;

import lombok.Data;

import java.util.List;

/**
 * @author: XuJiuCheng
 * @date: Created in 15:46 2021/12/13
 * @description:
 */
@Data
public class ReturnSupplyLineExtra2Cmd {

    /**
     * sku对应效期列表
     */
    private List<SkuInventoryInfoDetailCmd> skuInventoryInfoDetailList;
}
