//package com.sn.miaosha.test;
//
//import com.ncarzone.framework.insight.common.utils.JsonUtils;
//import com.ncarzone.scf.coord.common.util.OnsUtil;
//import com.ncarzone.scf.coord.facade.entity.returnsupply.CancelReturnSupplyOutboundFacadePo;
//import com.ncarzone.scf.coord.facade.entity.returnsupply.QueryReturnSupplyFulfilWithDetailPo;
//import com.ncarzone.scf.coord.facade.entity.returnsupply.ro.ReturnSupplyFulfilWithDetailRo;
//import com.ncarzone.scf.coord.facade.service.returnsupply.ReturnSupplyFulfilCommandFacade;
//import com.ncarzone.scf.coord.facade.service.returnsupply.ReturnSupplyFulfilQueryFacade;
//import com.qccr.knife.result.CommonStateCode;
//import com.qccr.knife.result.Result;
//import com.qccr.knife.result.Results;
//import com.qccr.message.objects.ncarzoneordercenter.OrdersRo;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.nio.charset.StandardCharsets;
//
///**
// * @author: XuJiuCheng
// * @date: Created in 11:15 2021/12/6
// * @description:
// */
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    @Resource
//    private ReturnSupplyFulfilQueryFacade returnSupplyFulfilQueryFacade;
//
//    @Resource
//    private ReturnSupplyFulfilCommandFacade returnSupplyFulfilCommandFacade;
//
//    /**
//     *
//     * @param queryReturnSupplyFulfilWithDetailPo 查询退供退供价接口测试
//     * @return String
//     */
//    @GetMapping(value = "/queryReturnSupplyFulfilWithDetail")
//    public String queryReturnSupplyFulfilWithDetail(QueryReturnSupplyFulfilWithDetailPo queryReturnSupplyFulfilWithDetailPo) {
//        Result<ReturnSupplyFulfilWithDetailRo> result = returnSupplyFulfilQueryFacade.queryReturnSupplyFulfilWithDetail(queryReturnSupplyFulfilWithDetailPo);
//        return JsonUtils.toJSONString(result);
//    }
//
//    /**
//     *
//     * @param cancelReturnSupplyOutboundFacadePo 取消退供接口测试
//     * @return String
//     */
//    @GetMapping(value = "/cancelReturnSupplyOutboundNotice")
//    public String cancelReturnSupplyOutboundNotice(CancelReturnSupplyOutboundFacadePo cancelReturnSupplyOutboundFacadePo) {
//        Result<Void> result = returnSupplyFulfilCommandFacade.cancelOutbound(cancelReturnSupplyOutboundFacadePo);
//        return JsonUtils.toJSONString(result);
//    }
//
//    @PostMapping("/testMsgSend")
//    public Result<String> send(@RequestParam("envType") Integer envType,
//                               @RequestParam("topic") String topic,
//                               @RequestParam("tag") String tag,
//                               @RequestParam("key") String key) {
//        String jsonFilePath = "/Users/xujiucheng/Desktop/idea-workspace/projects/2022.04.19/scfcoord/tools/scf-coord-deploy/src/main/java/com/ncarzone/scf/coord/deploy/json.txt";
//        String jsonStr = getDataFromJsonFile(jsonFilePath);
//        OrdersRo ordersRo = JsonUtil.parseObject(jsonStr, OrdersRo.class);
//        try {
//            String msgId = OnsUtil.sendMessage(envType, topic, tag, key, JsonUtils.toJSONString(ordersRo));
//            return Results.newSuccessResult(msgId);
//        } catch (Exception e) {
//            return Results.newFailedResult(CommonStateCode.FAILED, "消息发送失败");
//        }
//    }
//
//    public static String getDataFromJsonFile(String jsonFilePath) {
//        StringBuilder jsonString = new StringBuilder();
//        File file = new File(jsonFilePath);
//        if (file.exists()) {
//            BufferedReader bufferedReader = null;
//            try {
//                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
//                char[] chars = new char[8192];
//                int length;
//                while ((length = bufferedReader.read(chars)) != -1) {
//                    String temp = new String(chars, 0, length);
//                    jsonString.append(temp);
//                }
//            } catch (IOException e) {
//                System.out.println("=====获取数据异常=====" + e);
//            } finally {
//                if (null != bufferedReader) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException ex) {
//                        System.out.println("=======获取数据时，关闭流异常=======" + ex);
//                    }
//                }
//            }
//        }
//        return jsonString.toString();
//    }
//}
