package com.sn.miaosha.test;

import lombok.Data;

/**
 * @author: XuJiuCheng
 * @date: Created in 15:30 2021/12/13
 * @description:
 */
@Data
public class SkuInventoryInfoDetailCmd {

    /**
     * 效期类型
     */
    private Integer guaranteePeriodType;

    /**
     * 数量
     */
    private Integer skuQuantity;
}
