package com.sn.miaosha.io;

import org.assertj.core.util.Lists;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: XuJiuCheng
 * @date: Created in 14:45 2021/9/2
 * @description:
 */
public class BufferedReader {

    private static final String pathname = "/Users/xujiucheng/Desktop/idea-workspace/GitSelf/SpringBoot/src/test/java/com/sn/miaosha/io/test.txt";

    public static void main(String[] args) {
        concatSql(BufferedReader.getTexts());

    }

    /**
     * 获取文件中的内容放入List<String>
     */
    public static List<List<String>> getTexts() {
        List<List<String>> readLists = new ArrayList<>();
        try {
            System.out.println(pathname);
            readLists.add(Files.lines(Paths.get(pathname), Charset.defaultCharset())
                    .flatMap(line -> Arrays.stream(line.split("\n"))).collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return readLists;
    }

    /**
     *财务关账oc  orders表数据订正sql拼接
     * @param list
     */
    public static void concatSql(List<List<String>> list) {
        StringBuffer sb = new StringBuffer();
        for (List<String> s : list) {
            System.out.println(s.size());
            for (String str : s) {
                List<String> params = Lists.newArrayList(str.split(","));
                sb.append("UPDATE orders SET status = 56,business_finish_status= 1,business_finish_time= '")
                        .append(params.get(0)).append("',update_time= now(),update_person= '702693' WHERE user_id= ")
                        .append(params.get(1)).append(" AND no='").append(params.get(2)).append("';").append("\n");
            }
        }
        System.out.println(sb.toString());
    }

    public static void concatDubbo(List<List<String>> list) {
        StringBuffer sb = new StringBuffer();
        for (List<String> s : list) {
            System.out.println(s.size());
            for (String str : s) {
                List<String> params = Lists.newArrayList(str.split(","));
                sb.append("invoke com.qccr.ordercenter.facade.service.plain.PlainOrderFacade.businessFinish({\"class\":\"com.qccr.message.objects.ncarzoneordercenter.OrdersRo\", \"orderId\":")
                        .append(params.get(0)).append(", \"userId\":")
                        .append(params.get(1)).append(", \"norder\":{\"orderId\":").append(params.get(0))
                        .append(", \"userId\":").append(params.get(1)).append(",}})").append("\n");
            }
        }
        System.out.println(sb.toString());
    }

    public static List<String> getIdLists(List<String> list) {
        List<String> resultList = new ArrayList<>();
        list.forEach(idList->{
            resultList.add(idList);
        });
        return resultList;
    }

}
