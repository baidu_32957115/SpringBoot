package com.sn.miaosha.backdoor;

import java.io.Serializable;

public class OnsRestMessageDo implements Serializable {
    private static final long serialVersionUID = -3231205128033562471L;
    private String MsgId;
    private String OffsetId;
    private PropertyListPo PropertyList;

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String msgId) {
        MsgId = msgId;
    }

    public String getOffsetId() {
        return OffsetId;
    }

    public void setOffsetId(String offsetId) {
        OffsetId = offsetId;
    }

    public PropertyListPo getPropertyList() {
        return PropertyList;
    }

    public void setPropertyList(PropertyListPo propertyList) {
        PropertyList = propertyList;
    }
}
