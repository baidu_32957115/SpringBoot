package com.sn.miaosha.backdoor;

import lombok.Data;

@Data
public class MessagePropertyRo {
    private String Name;
    private String Value;
}
