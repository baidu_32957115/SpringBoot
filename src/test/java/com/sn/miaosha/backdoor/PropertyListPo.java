package com.sn.miaosha.backdoor;


import java.util.List;

public class PropertyListPo {
    private List<MessagePropertyRo> MessageProperty;

    public List<MessagePropertyRo> getMessageProperty() {
        return MessageProperty;
    }

    public void setMessageProperty(List<MessagePropertyRo> messageProperty) {
        MessageProperty = messageProperty;
    }
}
