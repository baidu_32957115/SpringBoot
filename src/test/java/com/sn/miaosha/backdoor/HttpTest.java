package com.sn.miaosha.backdoor;

import com.ncarzone.framework.insight.common.utils.JsonUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class HttpTest {

    public static void main(String[] args) {
        //消息key，逗号分隔
        String orderNoList = "OON227060013P63955";
        //消息对应的topic
        String topic = "MQ_NCZ-1-wms";
        //消息对应的tag
        String tag = "NCLC_STOCK_OUT_DETAILS";
        List<String> messageIds = new ArrayList<>();
        String getUrl = "https://pre-i0-nczgateway.carzone360.com/gateway/i0/cloudplatform/api/mq/msg/by-key?region_id=cn-shanghai&instance_id=MQ_INST_1846819814603408_BbGDtx5U&topic=" + topic + "&key_word=";
        Arrays.stream(orderNoList.split(",")).forEach(orderNo -> {
            String getResult = HttpRequest.doGet(getUrl + orderNo);
            try {
                TestResult testResult = JsonUtils.parseObject(getResult, TestResult.class);
                if (null != testResult) {
                    List<OnsRestMessageDo> onsRestMessageDoList = testResult.getData().getOnsRestMessageDo();
                    if (CollectionUtils.isNotEmpty(onsRestMessageDoList)) {
                        onsRestMessageDoList.stream().filter(x -> x.getPropertyList() != null).forEach(onsRestMessageDo -> {
                            List<MessagePropertyRo> messagePropertyRoList = onsRestMessageDo.getPropertyList().getMessageProperty();
                            //获取制定tag的msgid
                            Optional<MessagePropertyRo> messagePropertyRoOptional = messagePropertyRoList.stream().filter(x -> tag.equals(x.getValue())).findFirst();
                            if (messagePropertyRoOptional.isPresent() && StringUtils.isNotBlank(onsRestMessageDo.getMsgId())) {
                                messageIds.add(onsRestMessageDo.getMsgId());
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println("所有重试msgId列表：" + messageIds);
        long startTime = System.currentTimeMillis();
        for (String messageId : messageIds) {
            try {
                OnsUtil.consumeValidate(4, messageId, topic, "GID_NCZ-scfgoods");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long consuming = System.currentTimeMillis() - startTime;
        System.out.println("所有重试msgId列表重试完毕耗时：" + consuming);
    }
}
