package com.sn.miaosha.controller;


import com.sn.miaosha.backdoor.util.OnsUtil;
import com.sn.miaosha.entity.OutboundNoticeDo;
import com.sn.miaosha.mapper.OutboundNoticeMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: XuJiuCheng
 * @date: Created in 09:32 2022/1/19
 * @description:
 */
@Controller
@RequestMapping("/backdoor")
public class BackDoorController {

    private static final String VIEW_NAME = "freemarker/BackDoor";

    @Resource
    private OutboundNoticeMapper outboundNoticeMapper;

    @RequestMapping("/userInfos")
    @ResponseBody
    public ModelAndView userInfos(String outboundNoticeNos){
        ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        List<OutboundNoticeDo> result = new ArrayList<>();
        if (StringUtil.isNotEmpty(outboundNoticeNos)) {
            String[] list = outboundNoticeNos.split(",");
            for (String outboundNoticeNo : list) {
                OutboundNoticeDo outboundNoticeDo = new OutboundNoticeDo();
                outboundNoticeDo.setOutboundNoticeNo(outboundNoticeNo);
                result = outboundNoticeMapper.queryOutboundNoticeListByConditionFromDb(outboundNoticeDo);
            }
        }
        modelAndView.addObject("outboundInfos", result);
        modelAndView.addObject("outboundNoticeNos", outboundNoticeNos);
        return modelAndView;
    }

    @RequestMapping("/getOnsMessageIds")
    @ResponseBody
    public ModelAndView getOnsMessageIds(String topic, String tag, String keys){
        ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        List<String> messageIds = new ArrayList<String>();
        if (StringUtils.isNotEmpty(topic) && StringUtils.isNotEmpty(tag) && StringUtils.isNotEmpty(keys)) {
            messageIds = OnsUtil.getMessageIds(topic, tag, keys);
        }
        modelAndView.addObject("messageIds", messageIds);
        return modelAndView;
    }
}
