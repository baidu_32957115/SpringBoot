package com.sn.miaosha.backdoor.entity;


import java.util.List;

/**
 * @author xujiucheng
 */
public class PropertyListPo {
    private List<MessagePropertyRo> messageProperty;

    public List<MessagePropertyRo> getMessageProperty() {
        return messageProperty;
    }

    public void setMessageProperty(List<MessagePropertyRo> messageProperty) {
        this.messageProperty = messageProperty;
    }
}
