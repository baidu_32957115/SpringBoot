package com.sn.miaosha.backdoor.entity;

import lombok.Data;

/**
 * @author xujiucheng
 */
@Data
public class MessagePropertyRo {
    private String name;
    private String value;
}
