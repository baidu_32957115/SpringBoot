package com.sn.miaosha.backdoor.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xujiucheng
 */
@Data
public class TestResult implements Serializable {
     static final long serialVersionUID = -38626546303735413L;
     Integer code;
     String success;
     TestObject data;
}
