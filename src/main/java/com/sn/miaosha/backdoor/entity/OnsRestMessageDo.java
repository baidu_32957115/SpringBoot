package com.sn.miaosha.backdoor.entity;

import java.io.Serializable;

/**
 * @author xujiucheng
 */
public class OnsRestMessageDo implements Serializable {
    private static final long serialVersionUID = -3231205128033562471L;
    private String msgId;
    private String offsetId;
    private PropertyListPo propertyList;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getOffsetId() {
        return offsetId;
    }

    public void setOffsetId(String offsetId) {
        this.offsetId = offsetId;
    }

    public PropertyListPo getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(PropertyListPo propertyList) {
        this.propertyList = propertyList;
    }
}
