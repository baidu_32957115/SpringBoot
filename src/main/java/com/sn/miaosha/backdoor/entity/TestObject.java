package com.sn.miaosha.backdoor.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xujiucheng
 */
@Data
public class TestObject implements Serializable {
    private static final long serialVersionUID = -518357699526415093L;
    private List<OnsRestMessageDo> onsRestMessageDo;
}
