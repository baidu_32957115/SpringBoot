// ---补发出库消息 开始---
$(function () {
    //搜索框
    $('#outboundNoticeNos').focus(function() {
        if($(this).val() === '请输入出库通知单号') {
            $(this).val("");
        }
    });
    $('#outboundNoticeNos').blur(function(){
        if($(this).val() === "") {
            $(this).val('请输入出库通知单号');
        }
    });
    //除了表头（第一行）以外所有的行添加click事件.
    $("tr").first().nextAll().click(function () {
        //为点击的这一行切换样式bgRed里的代码：background-color:#FF0000;
        $(this).children().toggleClass("bgRed");
        //判断td标记的背景颜色和body的背景颜色是否相同;
        if ($(this).children().css("background-color") != $(document.body).css("background-color")) {
            //如果相同，CheckBox.checked=true;
            $(this).children().first().children().attr("checked", true);

        } else {
            //如果不同,CheckBox.checked=false;
            $(this).children().first().children().attr("checked", false);
        }
    });
});

function batchReSentOutboundFinishNotice() {
    
}

// ---补发出库消息 结束---
