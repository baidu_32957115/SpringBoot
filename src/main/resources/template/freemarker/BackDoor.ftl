<!DOCTYPE html>
<html lang="en">
<head>
  <title>BackDoor</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdn.staticfile.org/popper.js/1.15.0/umd/popper.min.js"></script>
  <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#backDoor1">补发出库完成消息</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#backDoor2">工具2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#backDoor3">工具3</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#backDoor4">工具4</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <!--批量补发出库完成消息-->
    <div id="backDoor1" class="container tab-pane active" style="margin-top: 20px;">
      <form action="/backdoor/getOnsMessageIds" method="post">
        <label>
          <input type="text" id="topic" class="form-control" placeholder="请输入查询消息topic" style="height: 30px;width: 240px;" name="topic" >
          <input type="text" id="tag" class="form-control" placeholder="请输入查询消息tag" style="height: 30px;width: 240px;" name="tag" >
          <input type="text" id="keys" class="form-control" placeholder="请输入查询消息keys，英文逗号隔开" style="height: 30px;width: 240px;" name="keys" >
        </label>
        <button type="submit" class="btn btn-primary btn-sm" style="height: 30px;margin-left: 10px;" onclick="queryOns()">查询</button>
      </form>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>messageId</th>
            <th>操作</th>
          </tr>
        </thead>
        <tbody>
          <#if messageIds?? && (messageIds?size>0)>
            <#list messageIds as messageId>
            <tr>
              <td>${messageId}</td>
              <td><button type="button" class="btn btn-primary btn-sm" style="height: 30px;margin-left: 10px;" onclick="reSentOns()">重试</button></td>
            </tr>
            </#list>
          <#else >
            <tr>
              <td colspan="4">没有用户信息!</td>
            </tr>
          </#if>
        </tbody>
      </table>
    </div>
    <!-- -->
    <div id="backDoor2" class="container tab-pane" style="margin-top: 10px;">

    </div>
    <!-- -->
    <div id="backDoor3" class="container tab-pane fade" style="margin-top: 10px;">

    </div>
    <!-- -->
    <div id="backDoor4" class="container tab-pane fade" style="margin-top: 10px;">

    </div>
  </div>
</div>
<script>
  $(function () {
    //搜索框
    $('#topic').focus(function() {
      if($(this).val() === '请输入查询消息topic') {
        $(this).val("");
      }
    });
    $('#topic').blur(function(){
      if($(this).val() === "") {
        $(this).val('请输入查询消息topic');
      }
    });
    $('#tag').focus(function() {
      if($(this).val() === '请输入查询消息tag') {
        $(this).val("");
      }
    });
    $('#tag').blur(function(){
      if($(this).val() === "") {
        $(this).val('请输入查询消息tag');
      }
    });
    $('#keys').focus(function() {
      if($(this).val() === '请输入查询消息keys，英文逗号隔开') {
        $(this).val("");
      }
    });
    $('#keys').blur(function(){
      if($(this).val() === "") {
        $(this).val('请输入查询消息keys，英文逗号隔开');
      }
    });
    // initTable();
  });

  function queryOns() {
    let topic = $('#topic').val();
    let tag = $('#tag').val();
    let keys = $('#keys').val();
    let params = {
      "topic": topic,
      "tag": tag,
      "keys": keys
    };
    $.ajax({
      url: '../backdoor/getOnsMessageIds',//后台接口地址
      type: 'get',//请求方式，一般为get、post
      dataType: 'json',//表示返回的数据必须为json，否则：会走下面error对应的方法。
      contentType: 'application/json;charset=utf-8',//如果要发送json字符串，此属性不能少；否则，会出现后台会报异常
      data: params,//将json对象转换为json字符串
      success: function (data) {
      },
      error: function () {
      }
    });
  }

  // 补发mq消息
  function reSentOns() {

  }

  // 页面初始化
  // function initTable() {
  //   //先销毁表格
  //   $('#cusTable').bootstrapTable('destroy');
  //   //初始化表格,动态从服务器加载数据
  //   $("#cusTable").bootstrapTable({
  //     method: "get",  //使用get请求到服务器获取数据
  //     url: "<c:url value='/SellServlet?act=ajaxGetSellRecord'/>", //获取数据的Servlet地址
  //     striped: true,  //表格显示条纹
  //     pagination: true, //启动分页
  //     pageSize: 1,  //每页显示的记录数
  //     pageNumber:1, //当前第几页
  //     pageList: [5, 10, 15, 20, 25],  //记录数可选列表
  //     search: false,  //是否启用查询
  //     showColumns: true,  //显示下拉框勾选要显示的列
  //     showRefresh: true,  //显示刷新按钮
  //     sidePagination: "server", //表示服务端请求
  //     //设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder
  //     //设置为limit可以获取limit, offset, search, sort, order
  //     queryParamsType : "undefined",
  //     queryParams: function queryParams(params) {   //设置查询参数
  //       return {
  //         pageNumber: params.pageNumber,
  //         pageSize: params.pageSize,
  //         orderNum: $("#orderNum").val()
  //       };
  //     },
  //     onLoadSuccess: function(){  //加载成功时执行
  //       layer.msg("加载成功");
  //     },
  //     onLoadError: function(){  //加载失败时执行
  //       layer.msg("加载数据失败", {time : 1500, icon : 2});
  //     }
  //   });
  // }
</script>
</body>
</html>